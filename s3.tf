// S3 creation
#
#
#
#
#
#
#
// Create Bucket
resource "aws_s3_bucket" "this-bucket" {
  count = var.number_of_users
  bucket = "${var.names_of_users[count.index]}-bucket923"

#   tags = {
#     Name        = "Bucket"
#     Environment = "Dev"
#   }
}


// Upload file to bucket

resource "aws_s3_object" "this-object" {
  count = var.number_of_users
  bucket = aws_s3_bucket.this-bucket[count.index].bucket             #"cloud-user-bucket923"
  key    = "test.txt"
  source = "./test.txt"
  depends_on = [aws_s3_bucket.this-bucket]
}


// Bucket policy

data "aws_iam_policy_document" "this-bucket-policy-document" {
  count = var.number_of_users
  statement {
    effect    = "Deny"
    principals {
      type = "AWS"
      identifiers = [ "*" ]
    }
    actions   = [             
                  "s3:*"
                ]
    resources = [
                  "arn:aws:s3:::${var.names_of_users[count.index]}-bucket923",
                  "arn:aws:s3:::${var.names_of_users[count.index]}-bucket923/*"
                ]
    condition {
      test = "StringNotLike"
      variable = "aws:username"
      values = ["${var.names_of_users[count.index]}","Cloud_Man"]
    }
  }
}


// Attach bucket policy to bucket - Association
resource "aws_s3_bucket_policy" "this-bucket-policy" {
  count = var.number_of_users
  bucket = aws_s3_bucket.this-bucket[count.index].id
  policy = data.aws_iam_policy_document.this-bucket-policy-document[count.index].json     
  depends_on = [aws_s3_bucket.this-bucket, aws_s3_object.this-object]
}


# // Attach bucket policy to bucket - Association
# resource "aws_s3_bucket_policy" "this-bucket-policy" {
#   bucket = aws_s3_bucket.this-bucket.id
#   policy = file("bucketpolicy.json")        
#   depends_on = [aws_s3_bucket.this-bucket, aws_s3_object.this-object]
# }