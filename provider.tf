// Provider

provider "aws" {
  region = "us-east-1"
}


// Backend 
terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "terraform-923"
    key            = "g4-s3-project/terraform.tfstate"
    region         = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-923"
  }
 }
