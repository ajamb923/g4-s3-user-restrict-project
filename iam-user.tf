// 1. Create IAM user
resource "aws_iam_user" "this-user" {
  count = var.number_of_users
  name = var.names_of_users[count.index]
}

# resource "aws_iam_access_key" "this-key" {
#   user = aws_iam_user.this-user.name 
# }


data "aws_iam_policy_document" "this-user-policy-document" {
  count = var.number_of_users
  statement {
    effect    = "Allow"
    actions   = [             
                  "s3:ListBucket",
                  "s3:GetObject"
                ]
    resources = [
                  "arn:aws:s3:::${var.names_of_users[count.index]}-bucket923",
                  "arn:aws:s3:::${var.names_of_users[count.index]}-bucket923/*"
                ]
  }

  statement {
    effect = "Allow"
    actions = [
                "s3:ListAllMyBuckets"
            ]
    resources = ["*"]
  }

  statement {
    effect = "Allow"
    actions = ["iam:ChangePassword"]
    resources = ["*"]
  }
}



// Attach Permission to user
resource "aws_iam_user_policy" "this-user-policy" {
  count = var.number_of_users
  name   = "${var.names_of_users[count.index]}-policy"
  user   = aws_iam_user.this-user[count.index].name
  policy = data.aws_iam_policy_document.this-user-policy-document[count.index].json
}


// Login Info
resource "aws_iam_user_login_profile" "this-user-login" {
  count = var.number_of_users
  user = aws_iam_user.this-user[count.index].name
  password_reset_required = true
}


## ----- Most likely to use:

output "password" {
  value = [ aws_iam_user_login_profile.this-user-login[*].password, aws_iam_user_login_profile.this-user-login[*].user]
}




// **********************************************************************************************************************************

## I WAS TRYING TO GET THE OUTPUT TO GIVE THE USERNAME-PASSWORD "FORMAT" BUT IT DIDNT WORK
## THE CODE ABOVE WORKS FINE THO, IT WILL CREATE USERS AND THEIR RESPECTIVE POLICIES 





# ##loop
# output "password" {
#   value = [for user in aws_iam_user_login_profile.this-user-login[*].password : user]
# }

# output "password" {
#   value = join("-", flatten([aws_iam_user_login_profile.this-user-login[*].user, "--User"]))
# }

# output "password" {
#   value = join("", flatten([aws_iam_user_login_profile.this-user-login[*].password, "id"]))
# }






