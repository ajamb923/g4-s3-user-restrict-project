# // All variables

variable "names_of_users" {
  type = list(string)  
  description = "The names of the users"
}


variable "number_of_users" {
    description = "Number of users to be created"
}