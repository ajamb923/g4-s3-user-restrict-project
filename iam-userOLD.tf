# // 1. Create IAM user
# resource "aws_iam_user" "this-user" {
#   count = var.number_of_users
#   name = var.names_of_users[count.index]
# }

# # resource "aws_iam_access_key" "this-key" {
# #   user = aws_iam_user.this-user.name 
# # }


# data "aws_iam_policy_document" "this-user-policy-document" {
#   count = var.number_of_users
#   statement {
#     effect    = "Allow"
#     actions   = [             
#                   "s3:ListBucket",
#                   "s3:GetObject"
#                 ]
#     resources = [
#                   "arn:aws:s3:::${var.names_of_users[count.index]}-bucket923",
#                   "arn:aws:s3:::${var.names_of_users[count.index]}-bucket923/*"
#                 ]
#   }

#   statement {
#     effect = "Allow"
#     actions = [
#                 "s3:ListAllMyBuckets"
#             ]
#     resources = ["*"]
#   }

#   statement {
#     effect = "Allow"
#     actions = ["iam:ChangePassword"]
#     resources = ["*"]
#   }
# }

# # data "aws_iam_policy_document" "this-user-policy-document" {
# #   statement {
# #     effect    = "Allow"
# #     actions   = [             
# #                   "s3:ListBucket",
# #                   "s3:GetObject"
# #                 ]
# #     resources = [
# #                   "arn:aws:s3:::cloud-user-bucket923",
# #                   "arn:aws:s3:::cloud-user-bucket923/*"
# #                 ]
# #   }

# #   statement {
# #     effect = "Allow"
# #     actions = [
# #                 "s3:ListAllMyBuckets"
# #             ]
# #     resources = ["*"]
# #   }

# #   statement {
# #     effect = "Allow"
# #     actions = ["iam:ChangePassword"]
# #     resources = ["*"]
# #   }
# # }


# // Attach Permission to user
# resource "aws_iam_user_policy" "this-user-policy" {
#   count = var.number_of_users
#   name   = "${var.names_of_users[count.index]}-policy"
#   user   = aws_iam_user.this-user[count.index].name
#   policy = data.aws_iam_policy_document.this-user-policy-document[count.index].json
# }

# # // Attach Permission to user
# # resource "aws_iam_user_policy" "this-user-policy" {
# #   name   = "cloud-user-policy"
# #   user   = aws_iam_user.this-user.name
# #   policy = data.aws_iam_policy_document.this-user-policy-document.json
# # }


# // Login Info
# resource "aws_iam_user_login_profile" "this-user-login" {
#   count = var.number_of_users
#   user = aws_iam_user.this-user[count.index].name
#   password_reset_required = true
# }


# output "password" {
#   value = aws_iam_user_login_profile.this-user-login[*].password 
# }

# # output "password" {
# #   value = aws_iam_user_login_profile.this-user-login[*].password 
# # }


# // 2. Create & Attach Identity Policy to user to allow access to Bucket.
# // 3. Create Bucket
# // 4. Create Bucket policy 

